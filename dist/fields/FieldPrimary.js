'use strict';

var React = require('react');
var FieldMixin = require('nb-helper').nbField;

var FieldPrimary = React.createClass({
  mixins: [FieldMixin],

  render: function () {
    var props = this.getFieldProperties();
    return React.createElement(
      'div',
      { ref: 'containerField', className: props.className },
      React.createElement(
        'label',
        { ref: 'labelField', className: 'control-label', htmlFor: props.htmlFor },
        props.label
      ),
      React.createElement(
        'div',
        { className: 'input-group' },
        React.createElement(
          'label',
          { htmlFor: props.htmlFor, className: 'input-group-addon' },
          React.createElement('i', { className: 'fa fa-text-width' })
        ),
        React.createElement('input', {
          id: props.htmlFor,
          ref: 'stringField',
          readOnly: true,
          defaultValue: props.value,
          type: 'text',
          className: 'form-control',
          placeholder: props.pĺace
        })
      )
    );
  }
});

module.exports = FieldPrimary;