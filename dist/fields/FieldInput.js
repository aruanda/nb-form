'use strict';

var React = require('react');

var FieldInput = React.createClass({
  componentDidUpdate: function () {
    var isNotDiffValue = Boolean(this.refs.input.value === this.props.value);
    if (isNotDiffValue) return;

    var value = [null, undefined].indexOf(this.props.value) === -1 ? this.props.value : '';
    this.refs.input.value = value;
  },

  componentChangeValue: function (e) {
    var value = e.target.value;
    var isNotDiffValue = Boolean(value === this.props.value);
    if (isNotDiffValue) return;

    this.props.onChange(value);
  },

  render: function () {
    var props = this.props;

    return React.createElement('input', {
      id: props.id,
      ref: 'input',
      defaultValue: props.value,
      onChange: this.componentChangeValue,
      type: props.type,
      style: props.style,
      className: props.className,
      placeholder: props.placeholder
    });
  }
});

module.exports = FieldInput;