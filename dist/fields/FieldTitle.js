'use strict';

var React = require('react');
var nbSlug = require('nb-slug');
var nbLink = require('nb-helper').nbLink;;
var FieldMixin = require('nb-helper').nbField;
var FieldInput = require('./FieldInput');

var FieldTitle = React.createClass({
  mixins: [FieldMixin],

  render: function () {
    var props = this.getFieldProperties();
    var pathSlug = props.field.pathSlug;
    var model = this.props.model;
    var id = model && model.id ? '-' + String(model.id) : '';
    var slug = pathSlug.concat(nbSlug(props.value) + id);
    var link;

    if (id) {
      link = React.createElement(
        'a',
        { href: nbLink(slug), target: '_blanck', title: props.value },
        nbLink(slug)
      );
    } else {
      link = React.createElement(
        'span',
        { className: 'text-light-blue' },
        nbLink(slug)
      );
    }

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom' },
      React.createElement(FieldInput, {
        type: 'text',
        className: 'form-control input-lg',
        placeholder: props.place.translate(),
        value: props.value,
        style: { border: '0' },
        onChange: this.registerValue
      }),
      props.message || React.createElement(
        'div',
        { className: 'box-body' },
        link
      )
    );
  }
});

module.exports = FieldTitle;