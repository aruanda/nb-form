/*globals $:false*/
'use strict';

var React = require('react');
var copy = require('copy-to-clipboard');
var Dropzone = require('react-dropzone');
var nbLink = require('nb-helper').nbLink;
var FieldMixin = require('nb-helper').nbField;
var nbConnection = require('nb-connection');
var LoadingModal = require('nb-modal').LoadingModal;

var FieldImages = React.createClass({
  mixins: [FieldMixin],

  getInitialState: function () {
    return {
      show: false,
      type: '',
      title: '',
      message: ''
    };
  },

  onDrop: function (files) {
    var comp = this;
    var state = this.state;
    var props = this.getFieldProperties();

    var fileTag = props.attr;
    var backend = nbConnection.getInstance();
    var params = { attr: fileTag };

    files.forEach(function (f) {
      f.field = fileTag + '/' + f.name;
    });

    state.show = true;
    state.type = 'warning';
    state.title = 'Aguarde';
    state.message = 'As fotos estão sendo enviadas para o servidor. O tempo dessa operação vai depender da qualidade de sua conexão.';

    this.setState(state, function () {
      backend.all('files').post(params, files).then(function (res) {
        var newListFiles = res.body[fileTag].concat(props.value || []);

        state.show = false;
        this.setState(state, function () {
          $(comp.refs.abaGaleria).click();
          this.registerValue(newListFiles);
        }.bind(this));
      }.bind(this));
    }.bind(this));
  },

  render: function () {
    var state = this.state;
    var props = this.getFieldProperties();
    if (!props.value) props.value = [];

    var dropZone;
    if (!props.readOnly) dropZone = React.createElement(
      Dropzone,
      { ref: 'dropzone', className: 'dropzone', onDrop: this.onDrop },
      React.createElement(
        'div',
        { className: 'form-control', style: { minHeight: '74px' } },
        props.place.translate()
      )
    );

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom' },
      React.createElement(
        'ul',
        { className: 'nav nav-tabs pull-right' },
        React.createElement(
          'li',
          null,
          React.createElement(
            'a',
            { href: '#galeria', ref: 'abaGaleria', 'data-toggle': 'tab', 'aria-expanded': 'false' },
            'Galeria'
          )
        ),
        React.createElement(
          'li',
          { className: 'active' },
          React.createElement(
            'a',
            { href: '#upload', 'data-toggle': 'tab', 'aria-expanded': 'true' },
            'Upload'
          )
        ),
        React.createElement(
          'li',
          { className: 'pull-left header' },
          React.createElement('i', { className: 'fa fa-image' }),
          ' Imagens anexadas'
        )
      ),
      React.createElement(
        'div',
        { className: 'tab-content' },
        React.createElement(
          'div',
          { className: 'tab-pane active', id: 'upload' },
          React.createElement(
            'div',
            { className: 'form-group' },
            dropZone
          )
        ),
        React.createElement(
          'div',
          { className: 'tab-pane overflowHidden', id: 'galeria' },
          React.createElement(
            'ul',
            { className: 'mailbox-attachments' },
            props.value.filter(function (file) {
              return !file.hasOwnProperty('removed') || !file.removed;
            }).map(function (file, index) {
              file.preview = nbLink('files/' + file.filetag + '/' + file.filename);

              var withoutName = !file.hasOwnProperty('name');
              if (withoutName) file.name = file.filename;

              var copyFile = function () {
                return copy(file.preview);
              };

              return React.createElement(
                'li',
                { key: index },
                React.createElement(
                  'span',
                  { className: 'mailbox-attachment-icon has-img' },
                  React.createElement('img', { src: file.preview, alt: file.name })
                ),
                React.createElement(
                  'div',
                  { className: 'mailbox-attachment-info' },
                  React.createElement(
                    'a',
                    { href: file.preview, target: '_blank', className: 'mailbox-attachment-name' },
                    React.createElement('i', { className: 'fa fa-camera' }),
                    file.name
                  ),
                  React.createElement(
                    'button',
                    { type: 'button', onClick: copyFile, className: 'btn btn-primary pull-right btn-xs' },
                    React.createElement('i', { className: 'fa fa-copy' }),
                    ' copiar url'
                  )
                )
              );
            })
          )
        )
      ),
      React.createElement(LoadingModal, {
        show: state.show,
        type: state.type,
        title: state.title,
        message: state.message
      })
    );
  }
});

module.exports = FieldImages;