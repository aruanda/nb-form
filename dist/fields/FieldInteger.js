'use strict';

var React = require('react');
var FieldMixin = require('nb-helper').nbField;

var FieldInteger = React.createClass({
  mixins: [FieldMixin],

  render: function () {
    var props = this.getFieldProperties();

    return React.createElement(
      'div',
      { ref: 'containerField', className: props.className },
      React.createElement(
        'label',
        { ref: 'labelField', className: 'control-label', htmlFor: props.htmlFor },
        props.label.translate()
      ),
      React.createElement(
        'div',
        { className: 'input-group' },
        React.createElement(
          'label',
          { htmlFor: props.htmlFor, className: 'input-group-addon' },
          React.createElement('i', { className: 'fa fa-sort-numeric-asc' })
        ),
        React.createElement('input', {
          id: props.htmlFor,
          ref: 'integerField',
          readOnly: props.readOnly,
          defaultValue: props.value,
          onChange: this.handleChange,
          onKeyDown: this.handleKeyDownOnlyInteger,
          type: 'text',
          className: 'form-control',
          placeholder: props.place.translate()
        })
      ),
      props.message
    );
  }
});

module.exports = FieldInteger;