/* globals MediumEditorTable: false */
'use strict';

var React = require('react');
var FieldMixin = require('nb-helper').nbField;
var FieldInputArea = require('./FieldInputArea');
var FieldMixinText = require('nb-helper').nbFieldText;;
var ReactMediumEditor = require('react-medium-editor');
var linksIds = 0;

var FieldText = React.createClass({
  mixins: [FieldMixin, FieldMixinText],

  componentDidMount: function () {
    var fieldTxt = this;
    fieldTxt.prepareTextEditor();
  },

  componentWillUnmount: function () {
    var fieldTxt = this;
    fieldTxt.destroyTextEditor();
  },

  render: function () {
    var props = this.getFieldProperties();
    var idHtml = 'id_html_'.concat(++linksIds);
    var idEditor = 'id_editor_'.concat(++linksIds);
    var idDesign = 'id_Design_'.concat(++linksIds);

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom' },
      React.createElement(
        'ul',
        { className: 'nav nav-tabs pull-right' },
        React.createElement(
          'li',
          null,
          React.createElement(
            'a',
            { href: '#'.concat(idEditor), 'data-toggle': 'tab', 'aria-expanded': 'false' },
            'Editor'
          )
        ),
        React.createElement(
          'li',
          null,
          React.createElement(
            'a',
            { href: '#'.concat(idHtml), 'data-toggle': 'tab', 'aria-expanded': 'false' },
            'HTML'
          )
        ),
        React.createElement(
          'li',
          { className: 'active' },
          React.createElement(
            'a',
            { href: '#'.concat(idDesign), 'data-toggle': 'tab', 'aria-expanded': 'true' },
            'Design'
          )
        ),
        React.createElement(
          'li',
          { className: 'pull-left header' },
          React.createElement('i', { className: 'fa fa-file-text-o' }),
          ' Texto'
        )
      ),
      React.createElement(
        'div',
        { className: 'tab-content' },
        React.createElement(
          'div',
          { className: 'tab-pane active', id: idDesign },
          React.createElement(
            'div',
            { className: 'form-group' },
            React.createElement(ReactMediumEditor, {
              tag: 'p',
              text: props.value,
              onChange: this.handleChangeEditor,
              options: {
                placeholder: { text: '' },

                toolbar: {
                  buttons: ['h2', 'h3', 'bold', 'italic', 'underline', 'unorderedlist', 'orderedlist', 'strikethrough', 'quote', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'outdent', 'indent', 'anchor', 'image', 'table']
                },

                buttonLabels: 'fontawesome',

                extensions: {
                  table: new MediumEditorTable()
                }
              }
            })
          ),
          props.message
        ),
        React.createElement(
          'div',
          { className: 'tab-pane', id: idHtml },
          React.createElement(
            'div',
            { className: 'form-group' },
            React.createElement(FieldInputArea, {
              rows: 5,
              className: 'form-control',
              placeholder: props.place.translate(),
              value: props.value,
              onChange: this.handleChangeEditor
            })
          ),
          props.message
        ),
        React.createElement(
          'div',
          { className: 'tab-pane', id: idEditor },
          React.createElement(
            'div',
            { className: 'form-group' },
            React.createElement('div', { ref: 'textEditor', style: { minHeight: '200px' }, dangerouslySetInnerHTML: { __html: props.value } })
          ),
          props.message
        )
      )
    );
  }
});

module.exports = FieldText;