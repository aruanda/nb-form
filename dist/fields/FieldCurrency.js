/*globals $:false*/
'use strict';

var React = require('react');
var numeral = require('numeral');
var FieldMixin = require('nb-helper').nbField;

var FieldCurrency = React.createClass({
  mixins: [FieldMixin],

  componentDidMount: function () {
    var field = this;
    var el = $(field.refs.currencyField);

    el.maskMoney({
      prefix: 'R$'
    }).off('keyup').on('keyup', function () {
      var value = String(el[0].value || '');
      if (!value.length) value = undefined;

      var isZero = !value || value.toLowerCase() === 'r$0,00'.toLowerCase();
      value = isZero ? 0 : numeral().unformat(value);

      field.registerValue(value);
    });
  },

  componentDidUpdate: function () {
    var props = this.getFieldProperties();
    var value;

    if (props.value) {
      props.value = parseFloat(props.value, 10);
      value = numeral(props.value).format('($0,0.00)');
    } else {
      value = '';
    }

    var isNotDiffValue = Boolean(this.refs.currencyField.value === value);
    if (isNotDiffValue) return;

    this.refs.currencyField.value = value;
  },

  componentWillUnmount: function () {
    $(this.refs.currencyField).maskMoney('destroy');
  },

  render: function () {
    var props = this.getFieldProperties();
    var value;

    if (props.value) {
      props.value = parseFloat(props.value, 10);
      value = numeral(props.value).format('($0,0.00)');
    }

    return React.createElement(
      'div',
      { className: 'box' },
      React.createElement(
        'div',
        { className: 'box-header with-border' },
        React.createElement(
          'h3',
          { className: 'box-title' },
          props.label.translate()
        )
      ),
      React.createElement(
        'div',
        { className: 'box-body' },
        React.createElement('input', {
          type: 'text',
          ref: 'currencyField',
          defaultValue: value,
          className: 'form-control',
          placeholder: props.place.translate(),
          style: { textTransform: 'uppercase' },
          'data-affixes-stay': true,
          'data-thousands': '.',
          'data-decimal': ',',
          maxLength: 21
        })
      ),
      props.message
    );
  }
});

module.exports = FieldCurrency;