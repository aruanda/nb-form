'use strict';

var React = require('react');
var FieldMixin = require('nb-helper').nbField;

var FieldTextarea = React.createClass({
  mixins: [FieldMixin],

  render: function () {
    var props = this.getFieldProperties();

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom' },
      React.createElement(
        'ul',
        { className: 'nav nav-tabs pull-right' },
        React.createElement(
          'li',
          { className: 'pull-left header' },
          React.createElement('i', { className: 'fa fa-file-text-o' }),
          props.label.translate()
        )
      ),
      React.createElement(
        'div',
        { className: 'tab-content' },
        React.createElement(
          'div',
          { className: 'tab-pane active' },
          React.createElement(
            'div',
            { className: 'form-group' },
            React.createElement('textarea', {
              className: 'form-control',
              rows: '3',
              value: props.value,
              placeholder: props.place.translate(),
              onChange: this.props.onChange || this.handleChange
            })
          ),
          props.message
        )
      )
    );
  }
});

module.exports = FieldTextarea;