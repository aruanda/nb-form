/*globals $:false*/
'use strict';

var React = require('react');
var FieldMixin = require('nb-helper').nbField;
var FieldSelect = require('./FieldSelect');
var moment = require('moment');
var nbHistory = require('react-router').browserHistory;

var FieldPublication = React.createClass({
  mixins: [FieldMixin],

  componentDidMount: function () {
    var field = this;
    var inputLaunch = $(field.refs.inputLaunch);

    inputLaunch.inputmask('dd/mm/yyyy', {
      'placeholder': 'dd/mm/aaaa'
    });

    inputLaunch.on('change', field.handleChangeDate);

    inputLaunch.datepicker({
      todayBtn: 'linked',
      language: 'pt-BR',
      autoclose: true,
      todayHighlight: true
    });
  },

  componentDidUpdate: function () {
    var props = this.props;

    var fieldDataTime = props.field.publication.fieldDateTime;
    var valueDateTime = props.model[fieldDataTime];
    var valueFormated = this.handleFormatDate(valueDateTime);

    var value = $(this.refs.inputLaunch).val();
    if (value === valueFormated) return;

    $(this.refs.inputLaunch).val(valueFormated);
  },

  componentWillUnmount: function () {
    var field = this;
    var inputLaunch = $(field.refs.inputLaunch);

    inputLaunch.off('change');
    inputLaunch.inputmask('destroy');
    inputLaunch.datepicker('destroy');
  },

  handleChangeStatus: function (status) {
    if (!this.isMounted()) return;
    var props = this.props;
    var fieldAttr = props.field.publication.fieldStatus;
    if (!status) status = undefined;
    if (status === this.props.model[fieldAttr]) return;
    this.registerValue(status, fieldAttr);
  },

  handleChangeDate: function () {
    var props = this.props;
    var fieldAttr = props.field.publication.fieldDateTime;
    var valueInput = $(this.refs.inputLaunch).val();

    var valueFormated = valueInput && moment(valueInput, 'DD/MM/YYYY').isValid() ? moment(valueInput, 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;

    if (valueFormated === this.props.model[fieldAttr]) return;

    this.registerValue(valueFormated, fieldAttr);
  },

  handleOnCancel: function () {
    nbHistory.goBack();
  },

  handleFormatDate: function (valueDateTime) {
    if (!valueDateTime) return '';

    var isInvalidValue = String(valueDateTime).toLowerCase() === 'invalid date';
    if (isInvalidValue) return '';

    var isInvalidDate = !moment(valueDateTime).isValid();
    if (isInvalidDate) return;

    return moment(valueDateTime).format('DD/MM/YYYY');
  },

  render: function () {
    var props = this.props;
    var nbProps = this.getFieldProperties();
    var statusLabel = nbProps.label.replace(nbProps.attr, props.field.publication.fieldStatus);
    var dateLabel = nbProps.label.replace(nbProps.attr, props.field.publication.fieldDateTime);

    var fieldDataTime = props.field.publication.fieldDateTime;
    var valueDateTime = props.model[fieldDataTime];
    var valueFormated = this.handleFormatDate(valueDateTime);

    var fieldStatus = props.field.publication.fieldStatus;
    var valueStatus = props.model[fieldStatus] ? props.model[fieldStatus] : props.field.publication.statusDefault;

    var options = props.field.publication.optionsStatus.map(function (status) {
      return {
        value: status,
        label: statusLabel.concat('.', status).translate()
      };
    });

    return React.createElement(
      'div',
      { className: 'box' },
      React.createElement(
        'div',
        { className: 'box-header with-border' },
        React.createElement(
          'h3',
          { className: 'box-title' },
          statusLabel.translate()
        )
      ),
      React.createElement(
        'div',
        { className: 'box-body' },
        React.createElement(
          'div',
          { className: 'form-group' },
          React.createElement(
            'label',
            { htmlFor: 'inputEmail3', className: 'control-label' },
            React.createElement('i', { className: 'fa fa-map-signs fontSize11pxMiddle' }),
            'Status:'
          ),
          React.createElement(FieldSelect, {
            className: 'form-control',
            placeholder: 'Status de publicação',
            options: options,
            value: valueStatus,
            onChange: this.handleChangeStatus
          })
        ),
        React.createElement(
          'div',
          { className: 'form-group' },
          React.createElement(
            'label',
            { htmlFor: 'inputEmail3', className: 'control-label' },
            React.createElement('i', { className: 'fa fa-calendar-check-o fontSize11pxMiddle' }),
            dateLabel.translate()
          ),
          React.createElement('input', {
            ref: 'inputLaunch',
            type: 'text',
            className: 'form-control',
            onChange: this.handleChangeDate,
            defaultValue: valueFormated
          })
        ),
        React.createElement(
          'div',
          { className: 'col-sm-12 form-group' },
          React.createElement(
            'button',
            { type: 'submit', className: 'btn btn-block btn-primary btn-lg' },
            'Salvar'
          )
        ),
        React.createElement(
          'div',
          { className: 'col-sm-12 text-center' },
          React.createElement(
            'a',
            { href: 'javascript:void(0);', onClick: this.handleOnCancel, className: 'text-muted' },
            'cancelar'
          )
        )
      )
    );
  }
});

module.exports = FieldPublication;