/*globals $:false*/
'use strict';

var React = require('react');
var FieldInput = require('./FieldInput');
var FieldSelect = require('./FieldSelect');
var FieldMixin = require('nb-helper').nbField;
var ContentImage = require('nb-content-image');
var ContentEditable = require('nb-content-editable');

var FieldSEO = React.createClass({
  mixins: [FieldMixin],

  handleInline: function (property, texto) {
    var props = this.getFieldProperties();

    var seoAttrs = props.value || {};
    seoAttrs[property] = texto;

    this.registerValue(seoAttrs);
  },

  handleSaveTwitterImage: function (img) {
    var image = $(img);

    this.handleInline('twitterImage', {
      src: image.attr('src'),
      alt: image.attr('alt'),
      width: image.css('width'),
      height: image.css('height')
    });
  },

  handleSaveTwitterTitle: function (text) {
    this.handleInline('twitterTitle', text);
  },

  handleSaveTwitterDescription: function (text) {
    this.handleInline('twitterDescription', text);
  },

  handleSaveFacebookImage: function (img) {
    var image = $(img);

    this.handleInline('facebookImage', {
      src: image.attr('src'),
      alt: image.attr('alt'),
      width: image.css('width'),
      height: image.css('height')
    });
  },

  handleSaveFacebookTitle: function (text) {
    this.handleInline('facebookTitle', text);
  },

  handleSaveFacebookDescription: function (text) {
    this.handleInline('facebookDescription', text);
  },

  handleSaveGoogleTitle: function (text) {
    this.handleInline('googleTitle', text);
  },

  handleSaveGoogleDescription: function (text) {
    this.handleInline('googleDescription', text);
  },

  handleSaveGoogleImage: function (img) {
    var image = $(img);

    this.handleInline('googleImage', {
      src: image.attr('src'),
      alt: image.attr('alt'),
      width: image.css('width'),
      height: image.css('height')
    });
  },

  handleKeywordsChange: function (keywords) {
    this.handleInline('keywords', keywords);
  },

  handleDescriptionChange: function (description) {
    this.handleInline('description', description);
  },

  handleRobotChange: function (robots) {
    this.handleInline('robots', robots);
  },

  render: function () {
    var props = this.getFieldProperties();
    var seoAttrs = props.value;

    if (!seoAttrs) {
      seoAttrs = {};
      props.value = seoAttrs;
    }

    var googImg = seoAttrs.googleImage ? seoAttrs.googleImage : {};
    var twitImg = seoAttrs.twitterImage ? seoAttrs.twitterImage : {};
    var faceImg = seoAttrs.facebookImage ? seoAttrs.facebookImage : {};

    var options = [{ value: 'index, follow', label: 'index,   follow' }, { value: 'noindex, nofollow', label: 'noindex, nofollow' }];

    return React.createElement(
      'div',
      null,
      React.createElement(
        'div',
        { className: 'nav-tabs-custom' },
        React.createElement(
          'ul',
          { className: 'nav nav-tabs pull-right' },
          React.createElement(
            'li',
            null,
            React.createElement(
              'a',
              { href: '#indexacao', 'data-toggle': 'tab', 'aria-expanded': 'false' },
              'Indexação'
            )
          ),
          React.createElement(
            'li',
            { className: 'active' },
            React.createElement(
              'a',
              { href: '#metatags', 'data-toggle': 'tab', 'aria-expanded': 'true' },
              'Meta tags'
            )
          ),
          React.createElement(
            'li',
            { className: 'pull-left header' },
            React.createElement('i', { className: 'fa fa-globe' }),
            ' SEO'
          )
        ),
        React.createElement(
          'div',
          { className: 'tab-content' },
          React.createElement(
            'div',
            { className: 'tab-pane active', id: 'metatags' },
            React.createElement(
              'div',
              { className: 'form-group' },
              React.createElement(
                'label',
                null,
                'Description'
              ),
              React.createElement(FieldInput, {
                type: 'text',
                className: 'form-control',
                placeholder: 'Descrição da página',
                value: seoAttrs.description,
                onChange: this.handleDescriptionChange
              })
            ),
            React.createElement(
              'div',
              { className: 'form-group' },
              React.createElement(
                'label',
                null,
                'Keywords'
              ),
              React.createElement(FieldInput, {
                type: 'text',
                className: 'form-control',
                placeholder: 'Palavras-chave da página',
                value: seoAttrs.keywords,
                onChange: this.handleKeywordsChange
              })
            )
          ),
          React.createElement(
            'div',
            { className: 'tab-pane', id: 'indexacao' },
            React.createElement(
              'div',
              { className: 'form-group' },
              React.createElement(
                'label',
                null,
                'Indexação'
              ),
              React.createElement(FieldSelect, {
                className: 'form-control',
                placeholder: 'Indexação da página',
                value: seoAttrs.robots,
                onChange: this.handleRobotChange,
                options: options
              })
            )
          )
        )
      ),
      React.createElement(
        'div',
        { className: 'nav-tabs-custom' },
        React.createElement(
          'ul',
          { className: 'nav nav-tabs pull-right' },
          React.createElement(
            'li',
            null,
            React.createElement(
              'a',
              { href: '#google', 'data-toggle': 'tab', 'aria-expanded': 'false' },
              'seo.google.label'.translate()
            )
          ),
          React.createElement(
            'li',
            null,
            React.createElement(
              'a',
              { href: '#twitter', 'data-toggle': 'tab', 'aria-expanded': 'false' },
              'seo.twitter.label'.translate()
            )
          ),
          React.createElement(
            'li',
            { className: 'active' },
            React.createElement(
              'a',
              { href: '#facebook', 'data-toggle': 'tab', 'aria-expanded': 'true' },
              'seo.facebook.label'.translate()
            )
          ),
          React.createElement(
            'li',
            { className: 'pull-left header' },
            React.createElement('i', { className: 'fa fa-rocket' }),
            'Midias Sociais'
          )
        ),
        React.createElement(
          'div',
          { className: 'tab-content' },
          React.createElement(
            'div',
            { className: 'tab-pane', id: 'google' },
            React.createElement(
              'div',
              { className: 'box-body' },
              React.createElement(
                'div',
                { className: 'col-xs-12 bg-red-active' },
                React.createElement('i', { className: 'fa fa-google-plus fontSize18emPadding10px10px10px0' }),
                React.createElement(
                  'span',
                  { className: 'midiaSocial' },
                  'seo.google.label'.translate()
                )
              ),
              React.createElement(
                'div',
                { className: 'col-xs-12 attachment-block clearfix padding15px' },
                React.createElement(ContentImage, {
                  src: googImg.src,
                  alt: googImg.alt,
                  width: googImg.width,
                  height: googImg.height,
                  defaultWidth: 1015,
                  defaultHeight: 668,
                  className: 'img-responsive',
                  onChange: this.handleSaveGoogleImage
                }),
                React.createElement(
                  'div',
                  { className: 'marginTop20px' },
                  React.createElement(
                    'h4',
                    { className: 'attachment-heading text-red' },
                    React.createElement(ContentEditable, {
                      onlyText: true,
                      html: seoAttrs.googleTitle || 'seo.google.title'.translate(),
                      onChange: this.handleSaveGoogleTitle
                    })
                  ),
                  React.createElement(
                    'div',
                    { className: 'attachment-tex' },
                    React.createElement(
                      'p',
                      null,
                      React.createElement(ContentEditable, {
                        onlyText: true,
                        html: seoAttrs.googleDescription || 'seo.google.description'.translate(),
                        onChange: this.handleSaveGoogleDescription
                      })
                    )
                  )
                )
              )
            )
          ),
          React.createElement(
            'div',
            { className: 'tab-pane', id: 'twitter' },
            React.createElement(
              'div',
              { className: 'box-body' },
              React.createElement(
                'div',
                { className: 'col-xs-12 bg-aqua' },
                React.createElement('i', { className: 'fa fa-twitter fontSize18emPadding10px10px10px0' }),
                React.createElement(
                  'span',
                  { className: 'midiaSocial' },
                  'seo.twitter.label'.translate()
                )
              ),
              React.createElement(
                'div',
                { className: 'col-xs-12 attachment-block clearfix padding15px' },
                React.createElement(ContentImage, {
                  src: twitImg.src,
                  alt: twitImg.alt,
                  width: twitImg.width,
                  height: twitImg.height,
                  defaultWidth: 1015,
                  defaultHeight: 668,
                  className: 'img-responsive',
                  onChange: this.handleSaveTwitterImage
                }),
                React.createElement(
                  'div',
                  { className: 'marginTop20px' },
                  React.createElement(
                    'h4',
                    { className: 'attachment-heading text-aqua' },
                    React.createElement(ContentEditable, {
                      onlyText: true,
                      html: seoAttrs.twitterTitle || 'seo.twitter.title'.translate(),
                      onChange: this.handleSaveTwitterTitle
                    })
                  ),
                  React.createElement(
                    'div',
                    { className: 'attachment-tex' },
                    React.createElement(
                      'p',
                      null,
                      React.createElement(ContentEditable, {
                        onlyText: true,
                        html: seoAttrs.twitterDescription || 'seo.twitter.description'.translate(),
                        onChange: this.handleSaveTwitterDescription
                      })
                    )
                  )
                )
              )
            )
          ),
          React.createElement(
            'div',
            { className: 'tab-pane active', id: 'facebook' },
            React.createElement(
              'div',
              { className: 'box-body' },
              React.createElement(
                'div',
                { className: 'col-xs-12 bg-blue-active' },
                React.createElement('i', { className: 'fa fa-facebook-official fontSize18emPadding10px10px10px0' }),
                React.createElement(
                  'span',
                  { className: 'midiaSocial' },
                  'seo.facebook.label'.translate()
                )
              ),
              React.createElement(
                'div',
                { className: 'col-xs-12 attachment-block clearfix padding15px' },
                React.createElement(ContentImage, {
                  src: faceImg.src,
                  alt: faceImg.alt,
                  width: faceImg.width,
                  height: faceImg.height,
                  defaultWidth: 1015,
                  defaultHeight: 668,
                  className: 'img-responsive',
                  onChange: this.handleSaveFacebookImage
                }),
                React.createElement(
                  'div',
                  { className: 'marginTop20px' },
                  React.createElement(
                    'h4',
                    { className: 'attachment-heading text-light-blue' },
                    React.createElement(ContentEditable, {
                      onlyText: true,
                      html: seoAttrs.facebookTitle || 'seo.facebook.title'.translate(),
                      onChange: this.handleSaveFacebookTitle
                    })
                  ),
                  React.createElement(
                    'div',
                    { className: 'attachment-tex' },
                    React.createElement(
                      'p',
                      null,
                      React.createElement(ContentEditable, {
                        onlyText: true,
                        html: seoAttrs.facebookDescription || 'seo.facebook.description'.translate(),
                        onChange: this.handleSaveFacebookDescription
                      })
                    )
                  )
                )
              )
            )
          )
        )
      )
    );
  }
});

module.exports = FieldSEO;