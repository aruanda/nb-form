'use strict';

var React = require('react');
var FieldInput = require('./FieldInput');
var FieldMixin = require('nb-helper').nbField;
var htmlForId = 0;

var FieldGroup = React.createClass({
  mixins: [FieldMixin],

  handleChangeValue: function (attr, value) {
    var field = this;
    if (!field.props.model) field.props.model = {};
    var model = field.props.model;
    model[attr] = value;
  },

  mountInput: function (input, index) {
    var group = this;
    var props = group.getFieldProperties();
    var model = group.props.model;

    var htmlFor = props.label.concat(input.attr, group.htmlForId);
    var inputValue = model.hasOwnProperty(input.attr) && model[input.attr] ? model[input.attr] : undefined;

    var inputChange = function (value) {
      group.handleChangeValue(input.attr, value);
    };

    return React.createElement(
      'div',
      { key: index, className: input.classBox },
      React.createElement(
        'label',
        { htmlFor: htmlFor },
        props.label.replace(props.attr, input.attr).translate()
      ),
      React.createElement(
        'div',
        { className: 'input-group' },
        React.createElement(
          'label',
          { htmlFor: htmlFor, className: 'input-group-addon' },
          React.createElement('i', { className: input.icon })
        ),
        React.createElement(FieldInput, {
          id: htmlFor,
          type: 'text',
          className: 'form-control',
          placeholder: props.place.replace(props.attr, input.attr).translate(),
          value: inputValue,
          onChange: inputChange
        })
      )
    );
  },

  render: function () {
    var group = this;
    group.htmlForId = ++htmlForId;
    var props = group.getFieldProperties();
    var inputs = group.props.field.group.inputs.map(group.mountInput);

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom' },
      React.createElement(
        'ul',
        { className: 'nav nav-tabs pull-right' },
        React.createElement(
          'li',
          { className: 'pull-left header' },
          React.createElement('i', { className: 'fa fa-user' }),
          props.label.translate()
        )
      ),
      React.createElement(
        'div',
        { className: 'tab-content overflowHidden' },
        inputs
      )
    );
  }
});

module.exports = FieldGroup;