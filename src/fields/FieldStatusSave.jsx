'use strict';

var React = require('react');
var FieldMixin = require('nb-helper').nbField;
var FieldSelect = require('./FieldSelect');
var nbHistory = require('react-router').browserHistory;

var FieldStatusSave = React.createClass({
  mixins: [FieldMixin],

  handleChangeStatus: function(status) {
    if (!this.isMounted()) return;
    var props = this.props;
    if (!status) status = undefined;
    if (status === this.props.model[props.field.attr]) return;
    this.registerValue(status);
  },

  handleOnCancel: function() {
    nbHistory.goBack();
  },

  render: function() {
    var props = this.props;
    var nbProps = this.getFieldProperties();

    var valueStatus = props.model[nbProps.attr];

    var options = props.field.optionsStatus.map(function(status) {
      return {
        value: status,
        label: nbProps.label.concat('.', status).translate()
      };
    });

    return (
      <div className="box">
        <div className="box-header with-border">
          <h3 className="box-title">
            { nbProps.label.translate() }
          </h3>
        </div>

        <div className="box-body">
          <div className="form-group">
            <FieldSelect
              className="form-control"
              placeholder={ nbProps.place.translate() }
              options={options}
              value={valueStatus}
              onChange={this.handleChangeStatus}
            />
          </div>

          {nbProps.message}

          <div className="col-sm-12 form-group">
            <button type="submit" className="btn btn-block btn-primary btn-lg">
              Salvar
            </button>
          </div>

          <div className="col-sm-12 text-center">
            <a href="javascript:void(0);" onClick={this.handleOnCancel} className="text-muted">
              cancelar
            </a>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = FieldStatusSave;
